Vision Statement
----------------

"*Plasma Mobile aims to become a complete software system for mobile
devices. It is designed to give privacy-aware users back the
full-control over their information and communication. Plasma Mobile
takes a pragmatic approach and is inclusive to 3rd party software,
allowing the user to choose which applications and services to use. It
provides a seamless experience across multiple devices. Plasma Mobile
implements open standards and it is developed in a transparent process
that is open for the community to participate in.*"

.. toctree::
   :maxdepth: 1
   :caption: General
   :name: sec-general

   Introduction
   FAQ

.. toctree::   
   :maxdepth: 1  
   :caption: Design
   :name: sec-design

   Design

.. toctree::
   :maxdepth: 1
   :caption: Flashing and Testing
   :name: sec-flashing

   FlashingNeon
   RunningApps
   TesterGuide

.. toctree::   
   :maxdepth: 1
   :caption: Development
   :name: sec-development

   Roadmap
   Code
   Contributing
   DevGuide
   AppDevelopment
   Porting
